import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BattleComponent } from './battle/battle.component';
import { PokemonComponent } from './battle/pokemon/pokemon.component';
import { MoveComponent } from './battle/move/move.component';

@NgModule({
  declarations: [
    AppComponent,
    BattleComponent,
    PokemonComponent,
    MoveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
