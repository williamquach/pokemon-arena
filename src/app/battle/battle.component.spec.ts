import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleComponent } from './battle.component';
import {Battle} from './battle.model';
import {Pokemon} from './pokemon/pokemon.model';

describe('BattleComponent', () => {
  let component: BattleComponent;
  let fixture: ComponentFixture<BattleComponent>;
  let view;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BattleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleComponent);
    component = fixture.componentInstance;
    view = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should display battle by default', () => {
    expect(view.innerHTML).toContain('No battle');
  });

  it('should ', () => {
    const firstPokemon = new Pokemon({
      name: 'firstPokemon',
      hp: 100,
      speed: 10,
      moves: []
    })
    const secondPokemon = new Pokemon({
      name: 'secondPokemon',
      hp: 1000,
      speed: 2,
      moves: []
    })
    component.battle = new Battle({
      name: 'La grande bataille',
      arena: 'Bourgpalette',
      firstPokemon,
      secondPokemon
    });

    expect(view.innerHTML).toContain('La grande bataille');

  });
});
