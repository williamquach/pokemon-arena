import {Component, OnInit} from '@angular/core';
import {Battle} from './battle.model';
import {Move} from './move/move.model';
import {Pokemon} from './pokemon/pokemon.model';

@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.css']
})
export class BattleComponent implements OnInit {

  battle: Battle;

  constructor() { }

  ngOnInit(): void {

    const pikachusMoves: Move[] = [
      new Move({ name: 'attaque éclair', power: 10, type: 'tonerre', accuracy: 3})
    ]
    const pikachu: Pokemon = new Pokemon({
      name: 'Pikachu',
      hp: 100,
      speed: 5,
      moves: pikachusMoves
    });

    const carapucesMoves: Move[] = [
      new Move({ name: 'attaque eau', power: 5, type: 'eau', accuracy: 2})
    ]
    const carapuce: Pokemon = new Pokemon({
      name: 'Carapuce',
      hp: 80,
      speed: 4,
      moves: carapucesMoves
    });

    this.battle = new Battle({
      name: 'Big battle',
      arena: 'Bourgpalette',
      firstPokemon: pikachu,
      secondPokemon: carapuce
    });

    setTimeout(async () => {
      this.battle.launch().then(() => console.log('end'));
    }, 1000);
  }
}
