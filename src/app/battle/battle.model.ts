import {Pokemon} from './pokemon/pokemon.model';

export interface IBattleProps {
    name: string;
    arena: string;
    firstPokemon: Pokemon;
    secondPokemon: Pokemon;
}

export class Battle implements IBattleProps {
    name: string;
    arena: string;
    firstPokemon: Pokemon;
    secondPokemon: Pokemon;
    fightingPokemon: Pokemon;
    waitingPokemon: Pokemon;
    round: number;
    moveCount: number;

    fightLogs: string;


  constructor(props: IBattleProps) {
      this.name = props.name;
      this.arena = props.arena;
      this.firstPokemon = props.firstPokemon;
      this.secondPokemon = props.secondPokemon;
      this.fightingPokemon = Pokemon.decideFirstBetween(props.firstPokemon, props.secondPokemon);
      this.waitingPokemon = this.fightingPokemon === this.firstPokemon ? this.secondPokemon : this.firstPokemon;
      this.round = 0;
      this.moveCount = 0;
      this.fightLogs = '';
  }

  async launch() {
    if(this.firstPokemon === undefined || this.secondPokemon === undefined) {
        throw new Error('At least one pokemon is missing in this battle!');
    }
    else {
      this.addToLogs('<br>================================<br>');
      this.addToLogs(`Battle starting : ${this.firstPokemon.name} vs ${this.secondPokemon.name}`);
      this.addToLogs(`${this.firstPokemon.name} : ${this.firstPokemon.hp}HP | ${this.secondPokemon.name} : ${this.secondPokemon.hp}HP `);

      const winner = await this.fighting();

      this.addToLogs(`${this.firstPokemon.name} : ${this.firstPokemon.hp}HP | ${this.secondPokemon.name} : ${this.secondPokemon.hp}HP `);
      return winner;
    }
  }

  async fighting(): Promise<Pokemon> {
    return new Promise(resolve => {
      const battleInterval = setInterval(() => {

        const attack = this.fightingPokemon.pickAttackRandomly();
        const lostHp = this.fightingPokemon.attacks(this.waitingPokemon, attack);

        this.addToLogs('<br>================================<br>');
        this.addToLogs(`${this.fightingPokemon.name}'s turn!`);
        this.addToLogs(`${this.fightingPokemon.name} is attacking ${this.waitingPokemon.name} with ${attack.name}`)
        this.addToLogs(`${this.waitingPokemon.name} lost ${lostHp}HP!`);
        this.addToLogs(`${this.waitingPokemon.name} is now at ${this.waitingPokemon.hp}HP!`);

        this.switchFighterPokemon();

        if(this.isOnePokemonDead()) {
          // Stop battle
          this.addToLogs('<br>================================<br>');
          this.addToLogs(`Battle finished : ${this.firstPokemon.name} vs ${this.secondPokemon.name}`);
          const winner = this.getWinner();
          resolve(winner);
          clearInterval(battleInterval);
        }
      }, 1000);
    });
  }


  isOnePokemonDead(): boolean {
        return this.firstPokemon.hp <= 0 || this.secondPokemon.hp <= 0;
    }

  getWinner(): Pokemon | undefined {
    if(this.firstPokemon.hp <= 0 && this.secondPokemon.hp <= 0) {
      this.addToLogs('None of the pokemon win, both died during the fight!');
      return undefined;
    }
    else {
      if(this.firstPokemon.hp <= 0 && this.secondPokemon.hp > 0) {
        this.addToLogs(`${this.secondPokemon.name} won the fight with ${this.secondPokemon.hp}HP against ${this.firstPokemon.name} (${this.firstPokemon.hp}HP)`);
        return this.secondPokemon;
      }
      if(this.firstPokemon.hp > 0 && this.secondPokemon.hp <= 0) {
        this.addToLogs(`${this.firstPokemon.name} won the fight with ${this.firstPokemon.hp}HP against ${this.secondPokemon.name} (${this.secondPokemon.hp}HP)`);
        return this.firstPokemon;
      }
    }
  }
    switchFighterPokemon(): void {
        const temporaryPokemon: Pokemon = this.fightingPokemon;
        this.fightingPokemon = this.waitingPokemon;
        this.waitingPokemon = temporaryPokemon;
    }

    addToLogs(newLogs: string) {
      this.fightLogs += newLogs + '<br>';
    }
}
