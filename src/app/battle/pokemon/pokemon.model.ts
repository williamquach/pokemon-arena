import {Move} from '../move/move.model';

export interface IPokemonProps {
    name: string;
    hp: number;
    speed: number;
    moves?: Move[];
}

export class Pokemon implements IPokemonProps {
    name: string;
    hp: number;
    speed: number;
    moves: Move[];

    /**
     * Decide which pokemon should start according to its qualities (speed, strength etc...)
     * @param firstPokemon first pokemon
     * @param secondPokemon second pokemon
     * @param random randomizer
     */
    static decideFirstBetween(firstPokemon: Pokemon, secondPokemon: Pokemon, random = Math.random): Pokemon {
        if(firstPokemon.speed === secondPokemon.speed) {
            console.log('Picking a random Pokemon since equal speed!');
            const array: Pokemon[] = [firstPokemon, secondPokemon];
            const randomIndex = Math.floor(random() * array.length);
            return array[randomIndex]
        }
        else if(firstPokemon.speed > secondPokemon.speed) {
            return firstPokemon;
        }
        else {
            return secondPokemon;
        }
    }

    constructor(props: IPokemonProps) {
        this.name = props.name;
        this.hp = props.hp;
        this.speed = props.speed;
        this.moves = props.moves || [];
    }

    /**
     * Attack the opponent
     * Returns damage lost
     * @param opponent opponent of actual pokemon
     * @param move : attacks used
     */
    attacks(opponent: Pokemon, move: Move): number {
        // console.log(`${this.name} is attacking ${opponent.name} with ${move.name}`)
        if(move.power > opponent.hp) {
            const hpBeforeAttack = opponent.hp;
            opponent.hp = 0;
            return hpBeforeAttack;
        }
        else {
            opponent.hp -= move.power;
            return move.power;
        }
    }

    /**
     * Selects an attack randomly
     */
    pickAttackRandomly(random = Math.random): Move { // INVERSION DE CONTROLE
        if(this.moves.length === 0) {
            console.log(`${this.name} has no attack.`)
            throw new Error(`${this.name} has no attack.`)
        }
        else {
            const randomIndex = Math.floor(random() * this.moves.length);
            return this.moves[randomIndex];
        }
    }
}
